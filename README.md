# PLUTO Language Plugin for VSCodium

This is a plugin for the domain-specific programming language PLUTO defined by
ECSS to make it easier to write and read space mission operations procedures.

The PLUTO language is defined [here](docs/ECSS-E-ST-70-32C31July2008.pdf).

## Installation

* To start using your extension with VSCodium IDE, copy it into the `<user home>/.vscode-oss/extensions` folder and restart the VSCodium

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/pluto-vscodium-plugin/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/pluto-vscodium-plugin

To contribute to the development of this package:

- ...

Helpful resources on package writing:

- [Syntax Highlight Guide](https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide]

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/!DYAKaIbRbFKLWmKDNn:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
